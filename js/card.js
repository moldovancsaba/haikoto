    import { initializeApp } from "https://www.gstatic.com/firebasejs/9.19.1/firebase-app.js";
    import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.19.1/firebase-analytics.js";
    // TODO: Add SDKs for Firebase products that you want to use
    // https://firebase.google.com/docs/web/setup#available-libraries
  
    // Your web app's Firebase configuration
    // For Firebase JS SDK v7.20.0 and later, measurementId is optional
    const firebaseConfig = {
      apiKey: "AIzaSyA4PnAxLrlPuE_vJu7FFzOzV9mXjDspkxk",
      authDomain: "haikoto.firebaseapp.com",
      databaseURL: "https://haikoto-default-rtdb.europe-west1.firebasedatabase.app",
      projectId: "haikoto",
      storageBucket: "haikoto.appspot.com",
      messagingSenderId: "97371421863",
      appId: "1:97371421863:web:9b7e4caab63ea7e445ef82",
      measurementId: "G-H3MC7MS1NQ"
    };
  
    // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    const analytics = getAnalytics(app);


// Listen for changes in database
dbRef.on("value", (snapshot) => {
  // Clear cards container
  cardsContainer.innerHTML = "";

  // Loop through data in snapshot
  snapshot.forEach((cardSnapshot) => {
    // Get data from snapshot
    const cardData = cardSnapshot.val();

    // Create card element
    const card = document.createElement("div");
    card.classList.add("card");

    // Create image element
    const img = document.createElement("img");
    img.src = cardData.image;
    img.alt = cardData.name + " Image";
    card.appendChild(img);

    // Create card-details element
    const cardDetails = document.createElement("div");
    cardDetails.classList.add("card-details");
    card.appendChild(cardDetails);

    // Create name element
    const name = document.createElement("h2");
    name.textContent = cardData.name;
    cardDetails.appendChild(name);

    // Create description element
    const description = document.createElement("p");
    description.textContent = cardData.description;
    cardDetails.appendChild(description);

    // Create card-footer element
    const cardFooter = document.createElement("div");
    cardFooter.classList.add("card-footer");
    cardDetails.appendChild(cardFooter);

    // Create price element
    const price = document.createElement("div");
    price.classList.add("price");
    price.textContent = "$" + cardData.price;
    cardFooter.appendChild(price);

    // Create Add to Cart button
    const addToCartButton = document.createElement("button");
    addToCartButton.classList.add("btn", "btn-primary");
    addToCartButton.textContent = "Add to Cart";
    cardFooter.appendChild(addToCartButton);

    // Add card to cards container
    cardsContainer.appendChild(card);
  });
});
